/* Oscar Gil
 * 12358
 * Laboratorio 3
 */
package Default;

import java.net.* ;

public final class WebServer {
    
    public static final String DOMINIO = "ooii.com";
    
    public static void main(String argv[]) throws Exception {
        
        // Set the port number.
        int port = 25;
        int nThreads = 10;
        
        MailProcessor mailProcessor = new MailProcessor();
        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("Running...");
        
        Repository repo = new Repository();
        
        ThreadPool threadPool = new ThreadPool(nThreads);
        threadPool.start();
        while(true){
            // Listen for a TCP connection request.
            Socket connectionSocket = serverSocket.accept();
            System.out.println("new Request");
            // Construct an object to process the HTTP request message.
            SmtpRequest request = new SmtpRequest(connectionSocket, repo, mailProcessor);
            threadPool.execute((Runnable) request);
        }    
    }
}