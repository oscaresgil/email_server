/* Oscar Gil
 * 12358
 * Laboratorio 3
 */
package Default;

/*
 * Copyright (c) 2004, Rob Gordon.
 */
// revised from oddjob

/**
 * Very basic implementation of a thread pool.
 * 
 * @author Rob Gordon.
 * @param <T>
 */
public class ThreadPool<T extends Runnable> {

    private final SocketQueue queue;
    private boolean closed = true;

    private int poolSize = 3;
    
    public ThreadPool(int poolSize){
        this.poolSize = poolSize;
        queue = new SocketQueue();
    }
    
    synchronized public void start() {
        if (!closed) {
            throw new IllegalStateException("Pool already started.");
        }
        closed = false;
        for (int i = 0; i < poolSize; ++i) {
            MyThread thread = new MyThread(i);
            thread.start();
        }
    }

    synchronized public void execute(T request) {
        if (closed) {
            throw new PoolClosedException();
        }
        queue.enqueue(request);
    }
    
    private class MyThread extends Thread {
        
        int id;
        
        private MyThread(int id){
            this.id = id;
        }

        @Override
        public void run() {
            while (true) {
                T request = (T) queue.dequeue();
                if (request == null) {
                    continue;
                } else{
                    try {
                        System.out.println("I am thread " + id);
                        request.run();
                        System.out.println("Thread " + id + " request done");
                    } catch (Throwable t) {
                        // ignore
                    }
                }
            }
        }
    }

    public void close() {
        closed = true;
        queue.close();
    }

    private static class PoolClosedException extends RuntimeException {

        PoolClosedException() {
            super("Pool closed.");
        }
    }
}
