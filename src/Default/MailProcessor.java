/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Default;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author oscar
 */
public class MailProcessor {
    
    ClientSmtp clientSmtp;
    ArrayList<String> usuarios;
    
    public MailProcessor(){
        clientSmtp = new ClientSmtp();
        usuarios = new ArrayList<>();
        try {

            FileReader reader = new FileReader("repository\\users.txt");
            BufferedReader buffer = new BufferedReader(reader);
            String line;
            while ((line = buffer.readLine()) != null) {
                usuarios.add(line);

            }
        } catch (IOException exception) {
            System.out.println("No hay usuarios");
        }
    }
    
    private ArrayList<String> getUsername(ArrayList<String> receivers){
        ArrayList<String> storeList = new ArrayList<>();
        String[] mailPieces;
        
        for (int i = 0; i < receivers.size(); i ++) {
            String name = receivers.get(i);
            name = name.replace("<", "").replace(">", "").toLowerCase();
            mailPieces = name.split("@");
            if (mailPieces[1].equals(WebServer.DOMINIO)){
                if (usuarios.contains(mailPieces[0])){
                    storeList.add(mailPieces[0]);
                }
                receivers.remove(i);
                i -= 1;
            }
            
        }
        return storeList;
    }
    
    public boolean processMail(Mail mail){
        ArrayList<String> storeList = getUsername(mail.getReceipts());
        System.out.println(storeList);
        storeList.stream().forEach((String name) -> {
            try {
                File file = new File("repository\\" + name + "\\" + mail.getName() +".txt");
                File fParent = file.getParentFile();
                if (!fParent.exists()) fParent.mkdirs();
                FileWriter write = new FileWriter(file, false);
                write.write(mail.toString());
                write.close();
                System.out.println(mail);
            } catch (IOException ex) {
                Logger.getLogger(SmtpRequest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        if (!mail.getReceipts().isEmpty()){
            String domain = mail.getReceipt(0);
            return clientSmtp.resend(mail, domain.substring(domain.indexOf("@")+1));
        }
        
        return true;
    }
}
