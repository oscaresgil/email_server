/* Oscar Gil
 * 12358
 * Laboratorio 3
 */
package Default;

/*
 * Copyright  2004, Rob Gordon.
 */

import java.util.LinkedList;

/**
 *
 * @author Rob Gordon.
 */
class SocketQueue<T> {

    private final LinkedList<T> list;
    private boolean closed = false;
    
    public SocketQueue(){
         list = new LinkedList<>();
    }

    synchronized public void enqueue(T request) {
        if (closed) {
            throw new ClosedException();
        }
        list.add(request);
        notify();
    }

    synchronized public T dequeue() {
        while (!closed && list.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        }
        return list.isEmpty() ? null : list.removeFirst();
    }

    synchronized public int size() {
        return list.size();
    }

    synchronized public void close() {
        closed = true;
        notifyAll();
    }

    synchronized public void open() {
        closed = false;
    }

    public static class ClosedException extends RuntimeException {

        ClosedException() {
            super("Queue closed.");
        }
    }
}
