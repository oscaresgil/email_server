/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Default;

import java.util.ArrayList;

/**
 *
 * @author oscar
 */
public class Mail {
    private final String from;
    private final ArrayList<String> receipts;
    private final String data;

    public Mail(String from, ArrayList<String> receipts, String data) {
        this.from = from;
        this.receipts = receipts;
        this.data = data;
    }

    public String getFrom() {
        return from;
    }

    public ArrayList<String> getReceipts() {
        return receipts;
    }
    
    public String getReceipt(int i){
        return receipts.get(i);
    }
    
    public int getReceiptsSize() {
        return receipts.size();
    }

    public String getData() {
        return data;
    }
    
    public String[] getDataLines(){
        return data.split("\n");
    }
    
    public String getName() {
        return data.substring(0,data.indexOf("\n"));
    }

    @Override
    public String toString() {
        String retorno = "From: "+from+"\n";
        retorno = receipts.stream().map((s) -> "To: "+s+"\n").reduce(retorno, String::concat);
        retorno += "Subject: "+getName()+"\n";
        retorno += "Data: "+data;
        return retorno;
    }
    
    
    
    
}
