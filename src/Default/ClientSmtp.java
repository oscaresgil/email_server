/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Default;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 *
 * @author oscar
 */
public class ClientSmtp {
    
    private final int PORT = 25;
    private final String CONNECT = "220";
    private final String OK = "250";
    private final String DATA = "354";
    private final String QUIT = "221";
    
    Socket clientSocket;
    
    public boolean resend(Mail mail, String ip){

        String response, responseCode;
        String[] data = mail.getDataLines();;
        int i = 0;
        int step = 0;
        
        try {
            clientSocket = new Socket(ip, PORT);
            InputStream is = clientSocket.getInputStream();
            DataOutputStream os = new DataOutputStream(clientSocket.getOutputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            do {
                response = br.readLine();
                if (null != response){
                    responseCode = response.substring(0,3);
                    
                    switch (step){
                        case 0:
                            if (CONNECT.equals(responseCode)){
                                os.writeBytes("helo\n");
                                step = 1;
                            } else clientSocket = new Socket(ip, PORT);
                            break;
                        case 1:
                            if (OK.equals(responseCode)){
                                os.writeBytes("mail from: <" + mail.getFrom() + "@" + WebServer.DOMINIO + ">\n");
                                step = 2;
                            } else os.writeBytes("helo\n");
                            break;
                        case 2:
                            if (OK.equals(responseCode)){
                                os.writeBytes("rcpt to: " + mail.getReceipt(i) + "\n");
                                i ++;
                            } else if (i == 0){
                                os.writeBytes("mail from: <" + mail.getFrom() + "@" + WebServer.DOMINIO + ">\n");
                            } else os.writeBytes("rcpt to: " + mail.getReceipt(i-1) + "\n");
                            if (i == mail.getReceiptsSize()){
                                step = 3;
                            }
                            break;
                        case 3:
                            if (OK.equals(responseCode)){
                                os.writeBytes("data\n");
                                step = 4;
                                i = 0;
                            } else os.writeBytes("rcpt to: " + mail.getReceipt(i-1) + "\n");
                            break;
                        case 4:
                            if (DATA.equals(responseCode)){
                                os.writeBytes(data[i]);
                                step = 5;
                                i ++;
                            } else os.writeBytes("data\n");
                            break;
                        case 5:
                            if (OK.equals(responseCode)){
                                os.writeBytes(data[i]);
                                i ++;
                            } else os.writeBytes(data[i-1]);
                            if (i == data.length){
                                os.writeBytes(".\n");
                                step = 6;
                            }
                            break;
                        case 6:
                            if (QUIT.equals(responseCode)){
                                step = 7;
                            } else os.writeBytes(".\n");
                            break;
                    }
                }
            } while (step != 7);
            clientSocket.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
        return true;
    }
}
