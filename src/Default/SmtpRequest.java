/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Default;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;

/**
 *
 * @author oscar
 */
public class SmtpRequest implements Runnable{
    
    final static String CRLF = "\r\n";
    Socket socket;
    MailProcessor mailProcessor;

    // Constructor
    public SmtpRequest(Socket socket, Repository repo, MailProcessor mailProcessor) throws Exception {
        this.socket = socket;
        this.mailProcessor = mailProcessor;
    }

    // Implement the run() method of the Runnable interface.
    public void run() {
        try {
            processRequest();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void processRequest() throws Exception {
        
        String request, mailFrom="", data="", requestLower="";
        int step = 0;
        ArrayList<String> receiversArray = new ArrayList<>();
        
        // Get a reference to the socket's input and output streams.
        InputStream is = socket.getInputStream();
        DataOutputStream os = new DataOutputStream(socket.getOutputStream());
        
        // Say Hello
        os.writeBytes("220 Oooooi. Bienvenido chatio... Saluda primero!\n");
        
        // Set up input stream filters.
        BufferedReader br = new BufferedReader(new InputStreamReader(is));;
       
        do {
            request = br.readLine();
            if (null != request){
                requestLower = request.toLowerCase();
                switch (step){
                    case 0:
                        if (requestLower.equals("helo")) {
                            step = 1;
                            os.writeBytes("250 Ahi esta aquel veeeee! Quien son men?\n");
                        }
                        else os.writeBytes("Chatio saluda primero\n");
                        break;
                    case 1:
                        if (requestLower.substring(0, 11).equals("mail from: ")){
                            mailFrom = request.substring(12, request.length()-1);
                            if (mailFrom.contains("@")) {
                                step = 2;
                                os.writeBytes("250 Que onda manin " + mailFrom + "\n");
                            }
                            else os.writeBytes("Correo invalido\n");
                        } else os.writeBytes("Envía quien eres\n");
                        break;
                    case 2:
                        if (requestLower.equals("data")){
                            if (!receiversArray.isEmpty()){
                                os.writeBytes("250 Envia tu mensaje\n");
                                step = 3;
                            }
                            else os.writeBytes("No se a quien enviarle\n");
                        }
                        else{
                            if (requestLower.substring(0,9).equals("rcpt to: ")){
                                String rcpt = request.substring(9,request.length()-1);
                                if (rcpt.contains("@")){
                                    os.writeBytes("250 Recibido\n");
                                    receiversArray.add(rcpt);
                                }
                                else os.writeBytes("Correo invalido\n");
                            }
                        }
                        break;
                    case 3:
                        if (requestLower.equals(".")){
                            step = 4;
                            os.writeBytes("250 Transaccion Completada. Orale!\n");
                        }
                        else data += request+"\r\n";
                        break;
                }
            }
        } while (!requestLower.equals("quit\n") || step != 4);

        os.close();
        br.close();
        socket.close();
        
        Mail mail = new Mail(mailFrom, receiversArray,data);
        System.out.println(mail);
        mailProcessor.processMail(mail);
    }
}
